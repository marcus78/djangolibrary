from django.urls import path
from .views import AuthorListView, AuthorDetailView

urlpatterns = [

    path('autors',AuthorListView.as_view(), name='author-list'),
    path('autors/<pk>', AuthorDetailView.as_view(), name='author-detail'),


]