from django.contrib import admin
from .models import Author, Publisher, Book

# Register your models here.
# Zawiera konfigurację panelu administratora


class AuthorAdmin(admin.ModelAdmin):
    search_fields = ['last_name','first_name'] # pole wyszukiwania po nazwisku, po imieniu
    ordering = ['last_name'] # sorowanie po czym

class BookAdmin(admin.ModelAdmin):
    search_fields = ['title'] # wyszukiwarka po tytule
    list_display = ['title', 'author','isbn','publisher'] # jakie elementy obiektu chcemy pokazac

admin.site.register(Author,AuthorAdmin) # zajerestrowanie klasy
admin.site.register(Book,BookAdmin) # zajerestrowanie klasy
admin.site.register(Publisher)
