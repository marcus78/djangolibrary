from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Author # <=> from shelf.models import Author
from .models import Book # <=> from shelf.models import Book

# Create your views here.
# widoki .zapomocą jego będziemy wyciągać dane z bazy i wrzucać do szablonu (templates)

class AuthorListView(ListView):
    model = Author


class BookListView(ListView):
    model = Book

class AuthorDetailView(DetailView):
    model = Author