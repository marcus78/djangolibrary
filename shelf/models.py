# from __future__ import unicode_literals, absolute_import
from django.db import models
from django.contrib.auth.models import User

# ORM - Object Relations Mapper - powala trzymać obiekty w bazie danych


class Publisher(models.Model):
    name = models.CharField(max_length=70)

    def __str__(self):
        return self.name


class Author(models.Model):
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return "{first_name} {last_name}".format(first_name=self.first_name,
                                                 last_name=self.last_name)

class Book(models.Model):

    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    isbn = models.CharField(max_length=20)

    def __str__(self):
        return self.title